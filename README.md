---
title: "Data Driven Marketing"
output: github_document
---
# Subject: BUSX 37803 (Summer 16)
## Scratch
### TBD
## Assignments in Data Driven Marketing
### [Assignment 1](src/main/rlang/assignment1.R)
### [Assignment 2](src/main/rlang/assignment2.R)
### [Final Assignment](src/main/rlang/final-assignment.R)